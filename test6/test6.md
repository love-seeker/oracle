# 项目实践

## 概述

Oracle的零食销售系统数据库是一个管理零食销售业务的软件系统。它包含了多个模块，如产品管理、订单管理、客户管理、库存管理、促销管理等。该系统能够帮助零售商或批发商实现对顾客订单、库存和促销活动的管理和跟踪，从而提高销售效率、降低成本、增加利润。

这个项目的背景是为了建立一个完善的零食销售系统，让零售商或批发商能够更加高效、方便地管理零食销售业务。通过Oracle的零食销售系统数据库，用户可以轻松地处理订单、管理库存、跟踪客户信息和促销活动等，从而提高企业的竞争力和市场份额。 

系统的优势：

1. 可以提高企业的销售效率：通过系统的订单处理功能，销售人员可以快速地处理顾客的订单，从而缩短销售周期，提高销售效率。

2. 对库存管理进行了优化：通过对库存的动态监控和预警功能，系统可以帮助企业准确把握库存水平，避免因过多或过少库存而导致的财务损失。

3. 帮助企业更好地了解客户：通过客户管理模块的功能，可以记录客户的基本信息、购买历史、偏好等信息，从而帮助企业更好地了解客户需求。

4. 促销活动管控的功能全面：系统可以支持不同种类的促销活动，如满减、打折等，同时可以对促销活动的效果进行跟踪和评估，帮助企业更好地决策。

系统的劣势：

1. 系统的复杂度较高：由于系统包含了多个模块，管理和运营的难度比较大，需要较高水平的专业技术人员维护和操作。

2. 需要大量的数据输入和维护：系统需要准确的产品、订单、库存和客户等信息，如果信息录入有误或不及时，会对整个系统的正常运营造成影响。

3. 成本较高：由于Oracle是商业数据库软件，使用和购买成本较高，对于中小型企业来说会有一定的压力。

设计表结构：

创建空间

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps1.jpg) 

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps2.jpg) 

用户表：

```sql
create table TB_USER
(
 id  INTEGER,
 no  VARCHAR2(50),
 pwd  VARCHAR2(50),
 name VARCHAR2(50),
 type CHAR(1)
)
```

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps3.jpg) 

商品分类表：

```sql
create table TB_CLASS
(
 id  INTEGER,
 name VARCHAR2(50)
)
```

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps4.jpg)+

创建商品表：

```sql
create table TB_COMMODITY
(
 id    INTEGER,
 product_name VARCHAR2(100),
 classify_id INTEGER,
 model   VARCHAR2(50),
 unit   VARCHAR2(50),
 market_value VARCHAR2(50),
 sales_price VARCHAR2(50),
 cost_price  VARCHAR2(50),
 img   VARCHAR2(100),
 introduce  VARCHAR2(500),
 num   INTEGER
)
```

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps5.jpg) 

创建商品订单表：

```sql
CREATE TABLE TB_ORDER1 (
order_id NUMBER(10),
product_id NUMBER(10),
quantity NUMBER(10),
price NUMBER(10,2),
CONSTRAINT order_details_primary_key PRIMARY KEY (order_id, product_id)
) 
```

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps6.jpg) 

 

数据初始化：

TB_USER：

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps7.jpg) 

TB_CLASS初始化

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps8.jpg) 

TB_CUSTOMER1初始化：

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps9.jpg) 

 

用户管理：

```sql
CREATE USER MyAdmin01 IDENTIFIED BY pwd;

GRANT CONNECT, RESOURCE, DBA TO MyAdmin;

CREATE USER MyUser01 IDENTIFIED BY pwd;

GRANT CONNECT, RESOURCE TO myUser01;

GRANT SELECT, INSERT, UPDATE, DELETE ON TB_CLASS TO myUser01;

GRANT SELECT, INSERT, UPDATE, DELETE ON TB_CUSTOMER1 TO myUser01;

GRANT SELECT, INSERT, UPDATE, DELETE ON TB_ORDERS TO myUser01;

GRANT SELECT, INSERT, UPDATE, DELETE ON TB_COMMODITY TO myUser01;
```

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps10.jpg) 

***\*PL/SQL设计\****

```sql
CREATE OR REPLACE PACKAGE myORDER AS
 -- 存储过程：创建订单
 PROCEDURE create_order(
  p_customer VARCHAR2,
  p_pro_name VARCHAR2,
  p_time DATE,
  p_num NUMBER
 );

 FUNCTION calculate_total_amount(
  p_order_id NUMBER
 ) RETURN NUMBER;

END myORDER;
CREATE OR REPLACE PACKAGE BODY myORDER AS
 -- 存储过程：创建订单
 PROCEDURE create_order(
  p_customer VARCHAR2,
  p_pro_name VARCHAR2,
  p_time DATE,
  p_num NUMBER
 ) AS

 BEGIN
  INSERT INTO TB_ORDERS05 (id, customer, pro_name, time, num)
  VALUES (TB_ORDERS05_SEQ.NEXTVAL, p_customer, p_pro_name, p_time, p_num);
 END create_order;
 FUNCTION calculate_total_amount(
  p_order_id NUMBER
 ) RETURN NUMBER AS
  v_total_amount NUMBER;
 BEGIN
  SELECT num * total_price INTO v_total_amount
  FROM TB_ORDERS05
  WHERE id = p_order_id;
 

  RETURN v_total_amount;
 EXCEPTION
  WHEN NO_DATA_FOUND THEN
   RETURN 0;
 END calculate_total_amount;
END myORDER;

```

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps11.jpg) 

 

![img](file:///C:\Users\chenzi\AppData\Local\Temp\ksohtml276\wps12.jpg) 

 

# ***\*备份方案设计：\****

一个良好的备份方案可以确保企业业务数据的可靠性和安全性，同时也是企业保障业务连续性的重要手段。以下是为零食销售系统设计的备份方案：

 

\1. 数据备份频率

  针对不同业务数据类型和重要程度，应设定不同的数据备份频率。例如，针对较为重要和频繁发生变化的业务数据，可以设置每天、每周甚至每小时备份一次；而对于相对稳定和不太关键的数据，则可以考虑更长时间间隔的备份。

\2. 备份方式

  我们建议使用多种方式进行备份，以减少备份数据丢失的风险。对于重要的数据库备份，可以选择使用物理备份、逻辑备份等方式进行备份；而对于文件和日志备份，建议使用云存储、外部硬盘等方式，以确保备份数据的存储安全。

\3. 数据恢复

  对于备份数据的存储和恢复过程，需要定期进行测试，以确保备份数据的可靠性和恢复效率。可以将备份数据恢复到一个测试环境中，进行数据验证和复原操作，以确保数据的完整性和准确性。

\4. 定期校验备份

  在备份完成之后，应该校验备份已经成功完成。不仅要验证备份过程是否有报错，还要确保备份后的数据的完整性和一致性。可以使用 MD5 校验等方式进行一致性校验。

\5. 隔离备份数据存储环境

  针对重要数据的备份，建议采用离线备份的方式，并借助加密算法进行加密保护，以防止数据泄露。同时，应该将备份数据存储在隔离的存储环境中，避免备份数据被其他系统覆盖或误删除。

以上是为零食销售系统设计的备份方案，备份方案应当根据实际情况和需求进行定制化。此外，在定期备份之外，建议也要随时做好日常数据备份和管理工作，以免数据丢失或被篡改。



