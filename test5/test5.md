# 实验5：包，过程，函数的用法

学号：201910414316

姓名：骆辉

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。 Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

```sql
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
```

![image-20230526150032415](C:\Users\chenzi\AppData\Roaming\Typora\typora-user-images\image-20230526150032415.png)

```sql

函数Get_SalaryAmount()测试：
SQL> select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	   10 Administration			     4400
	   20 Marketing 			    19000
	   30 Purchasing			    24900
	   40 Human Resources			     6500
	   50 Shipping				   156400
	   60 IT				    28800
	   70 Public Relations			    10000
	   80 Sales				   304500
	   90 Executive 			    58000
	  100 Finance				    51608
	  110 Accounting			    20308

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  120 Treasury
	  130 Corporate Tax
	  140 Control And Credit
	  150 Shareholder Services
	  160 Benefits
	  170 Manufacturing
	  180 Construction
	  190 Contracting
	  200 Operations
	  210 IT Support
	  220 NOC

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  230 IT Helpdesk
	  240 Government Sales
	  250 Retail Sales
	  260 Recruiting
	  270 Payroll

已选择 27 行。


过程Get_Employees()测试：
SQL> set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/SQL> 
101 Neena
108 Nancy
109 Daniel
110 John
111 Ismael
112 Jose Manuel
113 Luis
200 Jennifer
203 Susan
204 Hermann
205 Shelley
206 William

PL/SQL 过程已成功完成。
```

# 实验总结与体会

在实验的最后一个任务中，我需要创建一个函数 "GET_EMP_COUNT"。这个函数接受部门ID作为输入参数，并返回该部门的员工数量。在函数实现中，我学习了如何在函数中使用 SELECT 语句查询数据和如何将查询结果作为函数返回值。同时，我也掌握了 PL/SQL 中包的作用和用法。使用包可以将需要的函数、过程和变量封装在一起，便于管理和维护。

通过这次实验，我不仅学习了 PL/SQL 语言的基础和常用语法，还深入了解了在 Oracle 数据库中如何使用 PL/SQL 语言来完成特定功能。同时，我也对数据处理和管理工具的使用技能有了提升。这些知识和技能将对我今后在数据分析和处理中发挥重要作用，帮助我更好地应对数据相关任务和挑战。